<?php
// code qui recupere les informations
$start_time = microtime(TRUE);
$servV = shell_exec('apache2 -v  | head -n 1 | sed -n -e \'s/^.*Server version: //p\'');
$phpv = phpversion();
$mem_usage = memory_get_usage(true); 
$memoire = shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'");
$memoire2 = shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'");
$cpuUsage = shell_exec("top -bn 2 -d 0.01 | grep '^%Cpu' | tail -n 1 | gawk '{print $2+$4+$6}'");
$distro = shell_exec("sed -n 's/^ *PRETTY_NAME *= *//p' /etc/os-release");
$uptime = shell_exec('uptime -p');
$load = sys_getloadavg();
$cpuload = $load[0];
$free = shell_exec('free');
$free = (string)trim($free);
$free_arr = explode("\n", $free);
$mem = explode(" ", $free_arr[1]);
$mem = array_filter($mem, function ($value) {
	return ($value !== null && $value !== false && $value !== '');
});
$mem = array_merge($mem);
$memtotal = round($mem[1] / 1000000, 2);
$memused = round($mem[2] / 1000000, 2);
$memfree = round($mem[3] / 1000000, 2);
$memshared = round($mem[4] / 1000000, 2);
$memcached = round($mem[5] / 1000000, 2);
$memavailable = round($mem[6] / 1000000, 2);

$memusage = round(($memavailable / $memtotal) * 100);

$vhost = shell_exec("apache2ctl -S | grep -o -E 'alias (.*)|(namevhost|server) (.*)\s' | cut -d ' ' -f 2");

$phpload = round(memory_get_usage() / 1000000, 2);

$diskfree = round(disk_free_space(".") / 1000000000);
$disktotal = round(disk_total_space(".") / 1000000000);
$diskused = round($disktotal - $diskfree);

$diskusage = round($diskused / $disktotal * 100);

if ($memusage > 85 || $cpuload > 85 || $diskusage > 85) {
	$trafficlight = 'red';
} elseif ($memusage > 50 || $cpuload > 50 || $diskusage > 50) {
	$trafficlight = 'orange';
} else {
	$trafficlight = '#2F2';
}

$end_time = microtime(TRUE);
$time_taken = $end_time - $start_time;
$total_time = round($time_taken, 4);

$disque = shell_exec('df -H --output=size,used,avail,target | column -t | sort -n -k6n');
$process = shell_exec('ps -eo pid,comm,%cpu,%mem --sort=-%mem | head -n 10');
?>