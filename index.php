<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>SRMonitor - Surveillez votre système</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Outil de monitoring pour serveur" />
	<meta name="author" content="Patrice Andreani" />

	<link rel="icon" type="image/png" href="images/favicon.ico" />
	<!-- Bootstrap core CSS -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="jquery/jquery-3.7.1.min.js"></script>
</head>
<body>

	<?php
	require('code.php');
	?>

	<!-- script pour actualiser la div infos -->
	<script type="text/javascript">
		$(document).ready(function() {
			setInterval(function() {
				$("#infos").load(window.location.href + " #infos", function(response, status, xhr) {
					if (status == "error") {
						console.error("Erreur de chargement : " + xhr.status + " " + xhr.statusText);
					}
				});
			}, 3000);
		});
	</script>

	<!-- script pour actualiser la div disque -->
	<script type="text/javascript">
		$(document).ready(function() {
			setInterval(function() {
				$("#disque").load(window.location.href + " #disque");
			}, 15 * 60 * 1000);
		});
	</script>

	<!-- script pour actualiser la div processus -->
    <script type="text/javascript">
        $(document).ready(function() {
            setInterval(function() {
                $.ajax({
                    url: window.location.href,
                    success: function(data) {
                        var newContent = $(data).find("#processus").html();
                        $("#processus").html(newContent);
                    },
                    error: function(xhr) {
                        console.error("Erreur de chargement : " + xhr.status + " " + xhr.statusText);
                    }
                });
            }, 3000);
        });
    </script>

	<!-- script pour actualiser la div systeme -->
	<script type="text/javascript">
		$(document).ready(function() {
			setInterval(function() {
				$("#systeme").load(window.location.href + " #systeme", function(response, status, xhr) {
					if (status == "error") {
						console.error("Erreur de chargement : " + xhr.status + " " + xhr.statusText);
					}
				});
			}, 1800000); // 1800000 ms = 30 minutes
		});
	</script>

	<div class="container-fluid min-vh-100">

		<div class="row">
			<div class="col-3 bloc">
				<span class="badge bg-primary">
					<h4>SRMonitor</h4>Outil simple de monitoring pour serveur web
				</span><br><br>
			</div>

			<div class="col-3 bloc">
				<button type="button" class="btn btn-primary">
					Uptime <span class="badge bg-secondary">
						<h5><?php echo $uptime; ?></h5>
					</span>
				</button>
			</div>

			<div class="col-3 bloc">
				<button type="button" class="btn btn-primary">
					IP <span class="badge bg-secondary">
						<h5><?php echo  $_SERVER['SERVER_ADDR']; ?></h5>
					</span>
				</button>
			</div>

			<div class="col-3 bloc">
				<button type="button" class="btn btn-primary">
					Hostname <span class="badge bg-secondary">
						<h5><?php echo php_uname('n'); ?></h5>
					</span>
				</button>
			</div>

		</div>

		<div class="row">

		<div class="card-group">
				<div class="card bg-custom-1">
					<div class="card-header">
						<h4>
							<div class="alert alert-success" role="alert">
								<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-info-square-fill" viewBox="0 0 16 16">
									<path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm8.93 4.588-2.29.287-.082.38.45.083c.294.07.352.176.288.469l-.738 3.468c-.194.897.105 1.319.808 1.319.545 0 1.178-.252 1.465-.598l.088-.416c-.2.176-.492.246-.686.246-.275 0-.375-.193-.304-.533L8.93 6.588zM8 5.5a1 1 0 1 0 0-2 1 1 0 0 0 0 2z" />
								</svg>
								Informations
							</div>
						</h4>
					</div>

					<div class="card-body" id="infos">

						<div class="contenu">
							<p class="card-text">
							<p>
								<button type="button" class="btn btn-primary infos">
									Température <span class="badge bg-secondary">
										<h5><?php
											$f = fopen("/sys/class/thermal/thermal_zone0/temp", "r");
											$temp = fgets($f);
											echo round($temp / 1000) . 'C°';
											fclose($f); ?></h5>
									</span>
								</button>
							</p>

							<p>
								<button type="button" class="btn btn-primary infos">
									RAM <span class="badge bg-secondary">
										<h5><?php echo $memoire; ?>%</h5>
									</span>
								</button>
							</p>

							<p>
								<button type="button" class="btn btn-primary infos">
									CPU <span class="badge bg-secondary">
										<h5><?php echo $cpuUsage; ?>%</h5>
									</span>
								</button>
							</p>

							<p>
								<button type="button" class="btn btn-primary infos">
									Stockage <span class="badge bg-secondary">
										<h5><?php echo $diskusage; ?>%</h5>
									</span>
								</button>
							</p>
						</div>

					</div>
				</div>
			
				<div class="card bg-custom-1">
					<div class="card-header">
						<h4>
							<div class="alert alert-success" role="alert">
								<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-code-slash" viewBox="0 0 16 16">
									<path d="M10.478 1.647a.5.5 0 1 0-.956-.294l-4 13a.5.5 0 0 0 .956.294l4-13zM4.854 4.146a.5.5 0 0 1 0 .708L1.707 8l3.147 3.146a.5.5 0 0 1-.708.708l-3.5-3.5a.5.5 0 0 1 0-.708l3.5-3.5a.5.5 0 0 1 .708 0zm6.292 0a.5.5 0 0 0 0 .708L14.293 8l-3.147 3.146a.5.5 0 0 0 .708.708l3.5-3.5a.5.5 0 0 0 0-.708l-3.5-3.5a.5.5 0 0 0-.708 0z" />
								</svg>
								Système
							</div>
						</h4>
					</div>
					<div class="card-body" id="systeme">
						
						<div class="contenu card-text">
							<?php echo '<p><span class="description">' . 'Distribution :<br> ' . '</span>' . $distro . '</p>';
							echo '<p><span class="description">' . "Kernel :<br> " . '</span>';
							echo php_uname('r') . '</p>';
							echo '<p><span class="description">' . 'Version de PHP :<br> ' . '</span>' . phpversion() . '</p>'; ?>

							<table>
								<thead>
									<tr>
										<th colspan="2">Services</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td><?php echo $servV ?></td>
										<td><?php $apache2 = exec("systemctl is-active apache2");
											if (strpos($apache2, "inactive") === false) {
												echo "Actif";
											} else {
												echo "Inactif";
											} ?></td>
									</tr>

									<tr>
										<td><?php echo 'fail2ban ' ?></td>
										<td><?php $fail2ban = exec("systemctl is-active fail2ban");
											if (strpos($fail2ban, "inactive") === false) {
												echo "Actif";
											} else {
												echo "Inactif";
											} ?></td>
									</tr>

									<tr>
										<td><?php echo 'ssh' ?></td>
										<td><?php $sshs = exec("systemctl is-active sshd");
											if (strpos($sshs, "inactive") === false) {
												echo "Actif";
											} else {
												echo "Inactif";
											} ?></td>
									</tr>

									<tr>
										<td><?php echo 'ufw' ?></td>
										<td><?php $ufw = exec("systemctl is-active ufw");
											if (strpos($ufw, "inactive") === false) {
												echo "Actif";
											} else {
												echo "Inactif";
											} ?></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			
				<div class="card bg-custom-1">
					<div class="card-header">
						<h4>
							<div class="alert alert-success" role="alert">
								<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-hdd-fill" viewBox="0 0 16 16">
									<path d="M0 10a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v1a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-1zm2.5 1a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zm2 0a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1zM.91 7.204A2.993 2.993 0 0 1 2 7h12c.384 0 .752.072 1.09.204l-1.867-3.422A1.5 1.5 0 0 0 11.906 3H4.094a1.5 1.5 0 0 0-1.317.782L.91 7.204z" />
								</svg>
								Disque dur
							</div>
						</h4>
					</div>
					<div class="card-body" id="disque">

						<div class="contenu card-text">

							<?php $disque_lines = explode("\n", trim($disque)); ?>
							<?php foreach ($disque_lines as $line): ?>
    						<span class="result"><?php echo htmlspecialchars($line); ?></span><br>
  							<?php endforeach; ?>
							<br>
							<span class="description">Espace disque libre :</span> <span class="result"><?php echo $diskfree; ?> GB</span><br>
							<span class="description">Espace disque utilisé :</span> <span class="result"><?php echo $diskused; ?> GB</span><br>
							<span class="description">Espace disque total :</span> <span class="result"><?php echo $disktotal; ?> GB</span>
						</div>

					</div>
				</div>
			
				<div class="card bg-custom-1">
					<div class="card-header">
						<h4>
							<div class="alert alert-success" role="alert">
								<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-activity" viewBox="0 0 16 16">
									<path fill-rule="evenodd" d="M6 2a.5.5 0 0 1 .47.33L10 12.036l1.53-4.208A.5.5 0 0 1 12 7.5h3.5a.5.5 0 0 1 0 1h-3.15l-1.88 5.17a.5.5 0 0 1-.94 0L6 3.964 4.47 8.171A.5.5 0 0 1 4 8.5H.5a.5.5 0 0 1 0-1h3.15l1.88-5.17A.5.5 0 0 1 6 2Z" />
								</svg>
								Processus
							</div>
						</h4>
					</div>
					<div class="card-body" id="processus">

						<div class="contenu card-text">
							
								<?php $var = shell_exec('TERM=xterm /usr/bin/top -bn 1 | grep -v "top -" | grep -v "Tasks:" | grep -v "Cpu(s):" | grep -v "Mem :" | grep -v "Swap:" | awk \'{ printf("%-8s %-8s  %-8s  %-8s\n", $2, $9, $10, $12); }\' | head -n 15');
								echo "<pre>$var</pre>"; ?>
						</div>

					</div>
				</div>
				</div>
		</div>

		<div class="row">
			<div class="bloc">
				<div class="contenu">
					<button type="button" class="btn btn-primary infos">
						<h4>VirtualHost configurés :</h4><br>
						<span class="badge bg-secondary">
							<?php $vhostArray = explode("\n", trim($vhost));
							$vhostArray = array_unique($vhostArray);
							foreach ($vhostArray as $host) {
								if (!empty($host)) {
									echo '<span class="badge bg-secondary">';
									echo '<h5>' . htmlspecialchars($host) . '</h5>';
									echo '</span>';
								}
							}
							?>

						</span>
					</button>

				</div>
			</div>
		</div>
	</div>
	<div class="row">
	<div class="col-12 bloc">
	<footer>
		<div class="footer">
			<p>Développement : Patrice Andreani /\ Publié sous licence GPLv3 /\ Dépôt <a href="https://gitlab.com/andre0ani/srmonitor">GitLab</a></p>
		</div>
	</footer>
	</div>
	</div>

</body>
</html>