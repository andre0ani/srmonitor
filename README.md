# SRMonitor


## Outil simple de monitoring pour serveur web.

![<NOM_DU_FICHIER>](srmonitor.png)

**SRMonitor** est un outil simple et léger qui permet de surveiller l'état de son serveur web.

C'est un outil accessible en ligne qu'il suffit de déposer sur son serveur web.

Il utilise **Bootstrap** et ses icônes.

Code source disponible sur [Gitlab](https://gitlab.com/andre0ani/srmonitor)


Basé sur : https://github.com/jamesbachini/Server-Check-PHP